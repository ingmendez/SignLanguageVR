using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookatOffset : MonoBehaviour
{
    private Transform mainCam;
    public float yOffset = 0;

    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            yOffset--;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            yOffset++;
        }
        yOffset = Mathf.Clamp(yOffset, -360, 360);
        transform.LookAt(mainCam);
        transform.rotation = Quaternion.Euler(0, yOffset, 0);
    }
}
